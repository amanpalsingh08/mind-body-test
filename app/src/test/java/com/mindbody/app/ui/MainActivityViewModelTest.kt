package com.mindbody.app.ui

import android.content.Context
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.test.MavericksTestRule
import com.airbnb.mvrx.withState
import com.mindbody.app.data.repositories.MainRepository
import com.mindbody.app.models.CountryFlagModel
import com.mindbody.app.models.CountryInfoModel
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertTrue
import org.junit.ClassRule
import org.junit.Test

class MainActivityViewModelTest {

    private lateinit var viewModel: MainActivityViewModel

    private val context: Context = mockk(relaxed = true)

    private lateinit var state: MainActivityViewState

    private lateinit var repository: MainRepository

    @Test
    fun test_resetCountryInfo() {

        val countryInfo = CountryInfoModel(
            name = "test country",
            capital = "test capital",
            iso2 = "test iso2",
            iso3 = "test iso3"
        )

        repository = MainRepository()

        assertNotEquals(null, countryInfo)

        state = MainActivityViewState(
            countryDetail = countryInfo
        )

        viewModel = MainActivityViewModel(state, repository)

        viewModel.resetCountryInfo()

        withState(viewModel) { state ->
            assertEquals(state.countryDetail, null)
        }

    }


    @Test
    fun refresh_countryList_loading() {

        state = MainActivityViewState()

        repository = MainRepository()

        viewModel = MainActivityViewModel(state, repository)

        viewModel.getCountriesWithFlag()

        // verify that loading state has changed to true upon subscription
        withState(viewModel) {
            assertTrue(it.requestCountryFlag is Loading)
            assertEquals(it.listCountriesWithFlags, emptyList<CountryFlagModel>())
        }


    }

    companion object {
        @JvmField
        @ClassRule
        val mvrxTestRule = MavericksTestRule()
    }
}