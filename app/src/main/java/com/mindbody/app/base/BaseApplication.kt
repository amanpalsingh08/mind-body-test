package com.mindbody.app.base

import android.app.Application
import com.airbnb.mvrx.mocking.MockableMavericks

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        MockableMavericks.initialize(this)
    }

}