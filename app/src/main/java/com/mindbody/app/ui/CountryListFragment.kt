package com.mindbody.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.airbnb.epoxy.AsyncEpoxyController
import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksView
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.withState
import com.google.android.material.snackbar.Snackbar
import com.mindbody.app.R
import com.mindbody.app.common.simpleController
import com.mindbody.app.databinding.FragmentCountryListBinding
import com.mindbody.app.epoxyModel.listViewCountry
import com.mindbody.app.models.CountryFlagModel
import com.mindbody.app.ui.MainActivityViewModel.Companion.KEY_NAME

class CountryListFragment : Fragment(), MavericksView {

    private lateinit var binding: FragmentCountryListBinding
    private val viewModel: MainActivityViewModel by activityViewModel()
    private val epoxyController: AsyncEpoxyController by lazy { epoxyController() }

    private var viewLayout: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        if (viewLayout == null) {
            binding = FragmentCountryListBinding.inflate(inflater, container, false)
            viewLayout = binding.root
        }
        return viewLayout as View
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            recyclerView.setController(epoxyController)
        }
        viewModel.getCountriesWithFlag()
    }

    override fun invalidate() = withState(viewModel) { state ->
        binding.loading.isVisible = state.requestCountryFlag is Loading
        if (state.requestCountryFlag is Fail) {
            Snackbar.make(binding.recyclerView, state.requestCountryFlag.error.message ?: "", Snackbar.LENGTH_SHORT).show()
        }
        epoxyController.requestModelBuild()
    }

    private fun epoxyController() = simpleController(viewModel) { state ->

        if (state.listCountriesWithFlags.isNotEmpty()) {
            state.listCountriesWithFlags.forEach { obj ->
                listViewCountry {
                    id(obj.name)
                    data(obj)
                    clickListener { model, _, _, _ -> onItemClicked(model.data()) }
                }
            }
        }
    }

    private fun onItemClicked(data: CountryFlagModel) {
        viewModel.resetCountryInfo()
        findNavController().navigate(R.id.nav_country_info, Bundle().apply {
            putString(KEY_NAME, data.name)
        })
    }
}