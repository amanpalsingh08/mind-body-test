package com.mindbody.app.ui

import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksViewModel
import com.airbnb.mvrx.MavericksViewModelFactory
import com.airbnb.mvrx.Success
import com.airbnb.mvrx.ViewModelContext
import com.mindbody.app.data.repositories.MainRepository
import com.mindbody.app.extensions.asFlow
import com.mindbody.app.models.PostCountryModel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.launch

class MainActivityViewModel(
    state: MainActivityViewState, private val repository: MainRepository
) : MavericksViewModel<MainActivityViewState>(state) {

    companion object : MavericksViewModelFactory<MainActivityViewModel, MainActivityViewState> {
        const val KEY_NAME = "name"
        override fun create(
            viewModelContext: ViewModelContext, state: MainActivityViewState
        ) = MainActivityViewModel(state, MainRepository())
    }

    fun getCountriesWithFlag() = withState { state ->
        if (state.listCountriesWithFlags.isEmpty()) viewModelScope.launch {
            repository::getCountriesWithFlag.asFlow(

            ).execute {
                when (it) {
                    is Loading -> copy(requestCountryFlag = Loading())
                    is Fail -> {
                        it.error.printStackTrace()
                        copy(requestCountryFlag = Fail(it.error))
                    }
                    is Success -> {
                        update(it()).copy(requestCountryFlag = Success(it()))
                    }
                    else -> {
                        this
                    }
                }
            }
        }
    }

    fun getCapitalByCountryName(countryName: String) = withState { state ->
        viewModelScope.launch {
            repository::getCapitalByCountryName.asFlow(
                PostCountryModel(country = countryName)
            ).execute {
                when (it) {
                    is Loading -> copy(requestCountryInfo = Loading())
                    is Fail -> {
                        it.error.printStackTrace()
                        copy(requestCountryInfo = Fail(it.error))
                    }
                    is Success -> {
                        update(it()).copy(requestCountryInfo = Success(it()))
                    }
                    else -> {
                        this
                    }
                }
            }
        }
    }

    fun resetCountryInfo() = setState { copy(countryDetail = null) }


}