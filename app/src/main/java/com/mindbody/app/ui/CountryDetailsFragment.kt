package com.mindbody.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.airbnb.mvrx.Fail
import com.airbnb.mvrx.Loading
import com.airbnb.mvrx.MavericksView
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.withState
import com.google.android.material.snackbar.Snackbar
import com.mindbody.app.R
import com.mindbody.app.databinding.FragmentCountryDetailsBinding
import com.mindbody.app.ui.MainActivityViewModel.Companion.KEY_NAME

class CountryDetailsFragment : Fragment(), MavericksView {

    private lateinit var binding: FragmentCountryDetailsBinding
    private val viewModel: MainActivityViewModel by activityViewModel()

    private var viewLayout: View? = null
    private var countryName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        countryName = arguments?.getString(KEY_NAME)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        if (viewLayout == null) {
            binding = FragmentCountryDetailsBinding.inflate(inflater, container, false)
            viewLayout = binding.root
        }
        return viewLayout as View
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        countryName?.let { name ->
            viewModel.getCapitalByCountryName(name)
        }
    }

    override fun invalidate() = withState(viewModel) { state ->
        binding.loading.isVisible = state.requestCountryInfo is Loading
        if (state.requestCountryInfo is Fail) {
            Snackbar.make(binding.loading, state.requestCountryInfo.error.message ?: "", Snackbar.LENGTH_SHORT).show()
        }

        if (state.countryDetail != null) {
            val countryInfo = state.countryDetail
            binding.tvCountryName.text = getString(R.string.text_country, countryInfo.name)
            binding.tvCountryCapital.text = getString(R.string.text_capital, countryInfo.capital)
            binding.tvCountryIso2.text = getString(R.string.text_iso2, countryInfo.iso2)
            binding.tvCountryIso3.text = getString(R.string.text_iso3, countryInfo.iso3)
        }

    }
}