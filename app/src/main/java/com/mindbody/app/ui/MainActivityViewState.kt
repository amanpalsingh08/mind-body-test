package com.mindbody.app.ui

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MavericksState
import com.airbnb.mvrx.Uninitialized
import com.mindbody.app.models.CountryInfoModel
import com.mindbody.app.models.CountryFlagModel
import com.mindbody.app.models.ResultCountryInfo
import com.mindbody.app.models.ResultListCountryModel

data class MainActivityViewState(
    val requestCountryFlag: Async<ResultListCountryModel> = Uninitialized,
    val requestCountryInfo: Async<ResultCountryInfo> = Uninitialized,
    val listCountriesWithFlags: List<CountryFlagModel> = emptyList(),
    val countryDetail: CountryInfoModel? = null
) : MavericksState {

    fun update(response: ResultListCountryModel): MainActivityViewState {
        return copy(listCountriesWithFlags = response.listCountryFlag)
    }

    fun update(response: ResultCountryInfo): MainActivityViewState {
        return copy(countryDetail = response.capitalData)
    }
}