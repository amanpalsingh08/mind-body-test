package com.mindbody.app.epoxyModel

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.bumptech.glide.Glide
import com.mindbody.app.databinding.ViewListCountryFlagBinding
import com.mindbody.app.models.CountryFlagModel

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ListViewCountry @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val binding = ViewListCountryFlagBinding.inflate(LayoutInflater.from(context), this)

    @ModelProp
    fun setData(dataObj: CountryFlagModel) {
        binding.tvCountryName.text = dataObj.name
        Glide.with(this)
            .load("https://flagcdn.com/48x36/${dataObj.iso2.lowercase()}.png")
            .into(binding.ivFlag)
    }

    @CallbackProp
    fun setClickListener(listener: OnClickListener?) {
        binding.tvCountryName.setOnClickListener(listener)
    }

}