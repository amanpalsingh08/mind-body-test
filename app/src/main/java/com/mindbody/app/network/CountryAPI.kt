package com.mindbody.app.network

import com.mindbody.app.models.PostCountryModel
import com.mindbody.app.models.ResultCountryInfo
import com.mindbody.app.models.ResultListCountryModel
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

@JvmSuppressWildcards
interface CountryAPI {

    @GET("countries/flag/images")
    suspend fun getCountriesWithFlag(): ResultListCountryModel

    @POST("countries/capital")
    suspend fun getCapitalByCountryName(@Body model: PostCountryModel): ResultCountryInfo
}