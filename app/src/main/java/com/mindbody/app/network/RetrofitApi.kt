package com.mindbody.app.network

import com.mindbody.app.tools.GeneratedCodeConverters
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

class RetrofitApi() {

    private val baseUrl get() = "https://countriesnow.space/api/v0.1/"

    private var loggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    fun <T> createService(service: Class<T>, token: String? = null): T {
        val okHttpClient: OkHttpClient = OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).addInterceptor(Interceptor { chain ->
            val builder = chain.request().newBuilder()
            if (token != null) builder.header("Authorization", "Bearer $token")
            builder.header("Accept", "application/json")
            return@Interceptor chain.proceed(builder.build())
        }).build()

        val retrofit = Retrofit.Builder().client(okHttpClient).addConverterFactory(GeneratedCodeConverters.converterFactory()).baseUrl(baseUrl).build()
        return retrofit.create(service)
    }

}