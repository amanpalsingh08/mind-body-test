package com.mindbody.app.data.repositories

import android.content.Context
import com.mindbody.app.models.PostCountryModel
import com.mindbody.app.network.CountryAPI
import com.mindbody.app.network.RetrofitApi

class MainRepository(private val retrofitApi: RetrofitApi = RetrofitApi()) {


    /**
     * returns the service class instance
     */
    private val service: CountryAPI by lazy {
        retrofitApi.createService(CountryAPI::class.java)
    }

    suspend fun getCountriesWithFlag() = service.getCountriesWithFlag()

    suspend fun getCapitalByCountryName(model: PostCountryModel) = service.getCapitalByCountryName(model)

}