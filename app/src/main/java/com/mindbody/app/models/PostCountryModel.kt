package com.mindbody.app.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostCountryModel(
    @Json(name = "country") @field:Json(name = "country") val country: String
)
