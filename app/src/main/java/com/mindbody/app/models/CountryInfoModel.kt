package com.mindbody.app.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CountryInfoModel(
    @Json(name = "name") @field:Json(name = "name") val name: String,
    @Json(name = "capital") @field:Json(name = "capital") val capital: String,
    @Json(name = "iso2") @field:Json(name = "iso2") val iso2: String,
    @Json(name = "iso3") @field:Json(name = "iso3") val iso3: String
)