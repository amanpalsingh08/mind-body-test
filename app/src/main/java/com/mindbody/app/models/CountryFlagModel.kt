package com.mindbody.app.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CountryFlagModel(
    @Json(name = "name") @field:Json(name = "name") val name: String,
    @Json(name = "flag") @field:Json(name = "flag") val flag: String,
    @Json(name = "iso2") @field:Json(name = "iso2") val iso2: String,
    @Json(name = "iso3") @field:Json(name = "iso3") val iso3: String
)