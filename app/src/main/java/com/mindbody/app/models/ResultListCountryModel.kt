package com.mindbody.app.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResultListCountryModel(
    @Json(name = "error") @field:Json(name = "error") val error: Boolean,
    @Json(name = "msg") @field:Json(name = "msg") val msg: String,
    @Json(name = "data") @field:Json(name = "data") val listCountryFlag: List<CountryFlagModel> = emptyList()
)

